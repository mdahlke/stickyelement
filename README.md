Sticky Element

Stick an element (target) to the screen when specific element is reached (subject)

###Options  
| Option       | Type        | Default                  | Possible Values | Description                                                                                   |
|--------------|-------------|--------------------------|-----------------|-----------------------------------------------------------------------------------------------|
| subject      | HTMLElement | this                     |                 | Subject to watch for when target should become sticky                                         |
| target       | HTMLElement | this                     |                 | Element that you want to stick                                                                |
| placeholder  | Boolean     | false                    | true/false      | clone a placeholder to use, instead of the actual element                                     |
| keepContents | Boolean     | false                    | true/false      | Keep the contents of the placeholder???                                                       |
| offset       | Number      | 0                        |                 | Add an offset to the sticking point (if adding 10, it will be the offset of the subject + 10) |
| minWidth     | Number      | null                     |                 | Minimum width the window needs to be for stickyelement to run                                 |
| stickyClass  | String      | sticky-element-fixed     |                 | class given to the element when it becomes sticky                                             |
| beforeInit   | Function    | function(){return null;} |                 | Function to run before initialization                                                         |
| onStick      | Function    | function(){return null;} |                 | Function to run when the element becomes sitcky                                               |
| onUnStick    | Function    | function(){return null;} |                 | Function to run when the element unsticks                                                     |
| onResize     | Function    | function(){return null;} |                 | Function to run when the window resizes        


###Initialize
$(function(){
	$('#stick-me').stickyelement();
});